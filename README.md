## Ahead Technical Test

Test exercise developed with Laravel 7.3

Steps to run exercise:

1. Clone project into your desired folder
2. Run "composer install" to install required vendors
3. Copy **.env.example** file to **.env**
4. Edit **.env** file and setup database connection parameters
    
   ```
     DB_CONNECTION=mysql
     DB_HOST=mysql_host (usually: localhost)
     DB_PORT=3306
     DB_DATABASE=database_local
     DB_USERNAME=database_access_username
     DB_PASSWORD=database_access_password
   ```
     
5. Run "php artisan key:generate" to set application key
6. Run "php artisan:migrate" to create tables
7. Run "php artisan serve"

I recommend using [Vagrant Homestead](https://laravel.com/docs/7.x/homestead) or LAMP / WAMP server. 
