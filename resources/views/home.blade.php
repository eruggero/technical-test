@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }} @if(session()->has('message')) <small>(<span class="text-success">{{ session('message') }}</span>)</small> @endif</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link @if(session('tab') == 'personal') active @endif" data-toggle="pill" href="#personal">Personal Info</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('tab') == 'iban') active @endif" data-toggle="pill" href="#iban">IBAN</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(session('tab') == 'invoicing') active @endif" data-toggle="pill" href="#invoicing">Invoicing</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="personal" class="tab-pane fade in @if(session('tab') == 'personal') active show @endif">
                            @include('layouts.parts._personal')
                        </div>
                        <div id="iban" class="tab-pane fade in @if(session('tab') == 'iban') active show @endif">
                            @include('layouts.parts._iban')
                        </div>
                        <div id="invoicing" class="tab-pane fade in @if(session('tab') == 'invoicing') active show @endif">
                            @include('layouts.parts._invoicing')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@php( session()->forget('message') )
@php( session()->forget('tab') )
