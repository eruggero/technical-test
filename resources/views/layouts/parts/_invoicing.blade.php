<form id="invoicingForm" method="POST" action="{{ route('invoicing.update', Auth::user()->id) }}" class="mt-5">
    @csrf
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

        <div class="col-md-6">
            <input id="address"
                   type="text"
                   class="form-control @error('address') is-invalid @enderror"
                   name="address"
                   value="{{ old('address', Auth::user()->invoicing->address) }}">

            @error('address')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

        <div class="col-md-6">
            <input id="city"
                   type="text"
                   class="form-control @error('city') is-invalid @enderror"
                   name="city"
                   value="{{ old('city', Auth::user()->invoicing->city) }}">

            @error('city')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Province') }}</label>

        <div class="col-md-6">
            <input id="province"
                   type="text"
                   class="form-control @error('province') is-invalid @enderror"
                   name="province"
                   value="{{ old('province', Auth::user()->invoicing->province) }}">

            @error('province')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Postal Code') }}</label>

        <div class="col-md-6">
            <input id="post_code"
                   type="text"
                   class="form-control @error('post_code') is-invalid @enderror"
                   name="post_code"
                   value="{{ old('post_code', Auth::user()->invoicing->post_code) }}">

            @error('post_code')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Update') }}
            </button>
        </div>
    </div>
</form>
