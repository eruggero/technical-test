@push('styles')
    <style>
        #number {
            text-transform: uppercase;
        }
    </style>
@endpush

<form id="ibanForm" method="POST" action="{{ route('iban.update', Auth::user()->id) }}" class="mt-5">
    @csrf
    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
    <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('DNI/NIE') }}</label>

        <div class="col-md-6">
            <input id="dni"
                   type="text"
                   class="form-control @error('dni') is-invalid @enderror"
                   name="dni"
                   value="{{ old('dni', Auth::user()->iban->dni) }}" >

            @error('dni')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('IBAN') }}</label>

        <div class="col-md-6">
            <input id="number"
                   type="text"
                   class="form-control @error('iban') is-invalid @enderror"
                   name="iban"
                   value="{{ old('iban', Crypt::decryptString(Auth::user()->iban->iban)) }}"
                   required>

            @error('iban')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Update') }}
            </button>
        </div>
    </div>
</form>

@push('scripts')
    <script>
        $(document).ready(function() {
            $('#number').mask('SS00 0000 0000 0000 0000 0000 0000 0000 0000 0000', {
                placeholder: '____ ____ ____ ____ ____ ____ ____ ____ ____ ____'
            });
        });
    </script>
@endpush
