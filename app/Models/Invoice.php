<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoicing_data';

    protected $fillable = [
        'user_id', 'address', 'city', 'province', 'post_code'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
