<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'bank_data';

    protected $fillable = [
        'user_id', 'dni', 'iban'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
