<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get user related iban data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function iban()
    {
        return $this->hasOne(Bank::class);
    }

    /**
     * Get user related invoicing data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invoicing()
    {
        return $this->hasOne(Invoice::class);
    }
}
