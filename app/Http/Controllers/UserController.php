<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Update personal data
     *
     * @param UserUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateRequest $request) {
        $data = $request->all();

        $user = User::find($request->user_id);
        $user->update($data);

        return redirect()->route('home')->with(['tab' => 'personal', 'msg' => 'Personal data updated OK!']);
    }
}
