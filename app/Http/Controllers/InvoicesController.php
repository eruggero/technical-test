<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvoicingUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;

class InvoicesController extends Controller
{
    /**
     * Update invoice data
     *
     * @param InvoicingUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(InvoicingUpdateRequest $request) {
        $data = $request->all();

        $user = User::find($request->user_id);
        $invoicing = $user->invoicing;
        $invoicing->update($data);

        return redirect()->route('home')->with(['tab' => 'invoicing', 'msg' => 'Invoicing data updated OK!']);
    }
}
