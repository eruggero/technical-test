<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * Session variables are used to tabs
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (session()->has('tab')) {
            session()->flash('tab', session('tab'));

        } else {
            session()->flash('tab', 'personal');
        }

        if (session()->has('msg')) {
            session()->flash('message', session('msg'));
            session()->forget('msg');
        }

        return view('home');
    }
}
