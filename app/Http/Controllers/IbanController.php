<?php

namespace App\Http\Controllers;

use App\Http\Requests\IbanUpdateRequest;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;

class IbanController extends Controller
{
    /**
     * Update IBAN data encrypting account
     *
     * @param IbanUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(IbanUpdateRequest $request) {
        $data = $request->all();

        $user = User::find($request->user_id);
        $iban = $user->iban;

        $iban->iban = Crypt::encryptString($request->iban);
        $iban->dni = $request->dni;

        $iban->save();

        return redirect()->route('home')->with(['tab' => 'iban', 'msg' => 'Iban data updated OK!']);
    }
}
