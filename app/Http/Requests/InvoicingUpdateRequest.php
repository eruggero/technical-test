<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class InvoicingUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => ['sometimes', 'max:191'],
            'city' => ['sometimes', 'max:191'],
            'province' => ['sometimes', 'max:191'],
            'post_code' => ['sometimes', 'regex:/^[0-9]{5}$/']
        ];
    }

    /**
     * Override validation error redirect
     *
     * @param Validator $validator
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function failedValidation(Validator $validator){
        return redirect()->back()->withErrors($validator->errors())->withInput()->with('tab', 'invoicing');
    }
}
