<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Validation\Validator;

class IbanUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'iban' => ['required', 'string', 'min:29'],
            'dni' => ['sometimes', 'alpha_num', 'min:9']
        ];
    }

    /**
     * Override validation error redirect
     *
     * @param Validator $validator
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function failedValidation(Validator $validator){
        return redirect()->back()->withErrors($validator->errors())->withInput()->with('tab', 'iban');
    }
}
